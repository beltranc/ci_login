<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/auth_model', 'auth_model');
	}
	
	public function index(){
		if($this->session->has_userdata('is_admin_login')){
			redirect('admin/home');
		}else{
			redirect('admin/login');
		}
	}
	
	public function login(){
		if($_POST){
			$this->form_validation->set_rules('username', 'Usuario', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			
			if ($this->form_validation->run() == FALSE) {
					$this->load->view('admin/auth/login');
			}else {
				$data = array(
					'username' => $this->input->post('username'),
					'password' => $this->input->post('password')
				);
				
				$result = $this->auth_model->login($data);
				
				if ($result == TRUE) {
					$admin_data = array(
						'admin_id' => $result['id'],
						'name' => $result['firstname'],
						'is_admin_login' => TRUE
					);
					$dataLastLogin = array(
						'id_user'	=> $result['id'],
						'last_ip'	=> $_SERVER['REMOTE_ADDR'],
						'last_login'=> date('Y-m-d : h:m:s'),
					);
					
					$this->auth_model->registerLog($dataLastLogin);
					
					$this->session->set_userdata($admin_data);
					redirect(base_url('admin/home'), 'refresh');
				}else{
					$data['msg'] = 'Usuario o Password invalido!';
					$this->load->view('admin/auth/login', $data);
				}
			}
		}else{
			$this->load->view('admin/auth/login');
		}
	}
	
	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url('admin/login'), 'refresh');
	}
}