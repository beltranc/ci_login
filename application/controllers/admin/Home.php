<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller{
	public function __construct(){
		parent::__construct();
		//$this->load->model('admin/general_model', 'general_model');
	}
	
	public function index(){
		if($this->session->has_userdata('is_admin_login')){
			$data['title'] = 'Dashboard';
			$this->load->view('admin/layout',$data);
		}else{
			redirect('admin/login');
		}
	}
}